package com.example.usertaskmanagement;

import com.example.usertaskmanagement.model.Admin;
import com.example.usertaskmanagement.model.User;
import com.example.usertaskmanagement.repository.AdminAuthorityRepository;
import com.example.usertaskmanagement.repository.AdminRepository;
import com.example.usertaskmanagement.repository.PhoneNumberRepository;
import com.example.usertaskmanagement.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@RequiredArgsConstructor
public class UserTaskManagementApplication implements CommandLineRunner {
    private final AdminRepository adminRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private  final PhoneNumberRepository phoneNumberRepository;
    public static void main(String[] args) {
        SpringApplication.run(UserTaskManagementApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        adminRepository.findAll().forEach(System.out::println);
    }
}
