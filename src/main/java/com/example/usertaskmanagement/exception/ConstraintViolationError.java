package com.example.usertaskmanagement.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
public class ConstraintViolationError {
    private String property;
    private String message;
}
