package com.example.usertaskmanagement.exception;

public enum ErrorCode {
    USER_ALREADY_PRESENT,
    PASSWORDS_DIDNT_MATCH
}
