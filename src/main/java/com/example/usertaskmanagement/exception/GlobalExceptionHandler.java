package com.example.usertaskmanagement.exception;

import com.example.usertaskmanagement.service.TranslationService;
import liquibase.license.LicenseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {


    private final TranslationService translationService;


    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorResponseDTO> handleBadRequestException(BadRequestException ex, WebRequest req) {
        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
           ex.printStackTrace();
           return ResponseEntity.status(400).body(ErrorResponseDTO.builder()
                           .status(400)
                           .title("Exception")
                           .details(translationService.findByKey(ex.getErrorCode().name(), lang, ex.getArguments()))
                   .build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDTO> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex,
                                                                                  WebRequest req) {
        ex.printStackTrace();
        ErrorResponseDTO response = ErrorResponseDTO.builder()
                .status(400)
                .title("Exception")
                .build();
        ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .forEach(error -> {
                    Map<String, String> data = response.getData();
                    data.put(error.getField(), error.getDefaultMessage());
                });
        return ResponseEntity.status(400).body(response);
    }

}
