package com.example.usertaskmanagement.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String organizationName;
    @OneToOne(cascade = CascadeType.ALL)
//    @ToString.Exclude
//    @JsonIgnore
    Address address;

    @OneToMany(mappedBy = "organization", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @ToString.Exclude
    Set<PhoneNumber> phoneNumbers = new HashSet<>();
}
