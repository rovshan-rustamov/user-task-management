package com.example.usertaskmanagement.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@ToString
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String title;
    String description;

    @JsonFormat(pattern = "dd-MM-yyyy:hh-mm", shape = JsonFormat.Shape.STRING)
    String deadline;
    @Enumerated(EnumType.STRING)
    Status status;
}
