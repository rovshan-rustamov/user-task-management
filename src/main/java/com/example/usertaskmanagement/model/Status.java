package com.example.usertaskmanagement.model;


public enum Status {
    New, //unassigned
    InProgress, //was assigned and user started the task
    Completed //task finished
}
