package com.example.usertaskmanagement.model.authority;

public enum UserAuthority {
    ADMIN,
    USER
}
