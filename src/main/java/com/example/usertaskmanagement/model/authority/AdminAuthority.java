package com.example.usertaskmanagement.model.authority;

public enum AdminAuthority {
    SUPER_ADMIN,
    ADMIN
}
