package com.example.usertaskmanagement.model;

import com.example.usertaskmanagement.model.authority.GrantedAdminAuthority;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

@Getter
@Setter
@ToString
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
//@NamedEntityGraph(name = "Admin.organization", attributeNodes = {
//        @NamedAttributeNode("organization")
//})
public class Admin implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String username;
    String email;
    String password;

    @OneToOne( cascade = CascadeType.ALL)
    Organization organization;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "admin_authorities",
            joinColumns = @JoinColumn(name = "admin_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
    List<GrantedAdminAuthority> authorities = new ArrayList<>();


    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
