package com.example.usertaskmanagement.configuration.security;

import com.example.usertaskmanagement.model.Admin;
import com.example.usertaskmanagement.model.User;
import com.example.usertaskmanagement.model.authority.GrantedAdminAuthority;
import com.example.usertaskmanagement.model.authority.GrantedUserAuthority;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class JwtService {
    private final String AUTHORITY = "authority";
    private final String USERNAME = "username";
    private final String EMAIL = "email";
    private final String ORGANIZATION = "organization";



    @Value("${spring.security.secret}")
    private String secret;
    @Value("${security.token.expirationTime}")
    private Long expTimeInMinutes;
    private Key key;

    @PostConstruct
    public void init(){
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(secret);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueTokenForAdmin(Admin admin){
        return Jwts.builder()
                .setSubject(admin.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(expTimeInMinutes))))
                .setHeader(Map.of("type","JWT"))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim(AUTHORITY, admin.getAuthorities().stream().
                        map(GrantedAdminAuthority::getAuthority).collect(Collectors.toList()))
                .claim(EMAIL, admin.getEmail())
                .claim(ORGANIZATION, "null for now")
                .compact();
    }

    public String issueTokenForUser(User user){
        return Jwts.builder()
                .setSubject(user.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(expTimeInMinutes))))
                .setHeader(Map.of("type","JWT"))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim(AUTHORITY, user.getAuthorities().stream().
                        map(GrantedUserAuthority::getAuthority).collect(Collectors.toList()))
                .claim(EMAIL, user.getEmail())
                .claim("name",user.getName())
                .claim("surname",user.getSurname())
                .compact();
    }





//    public Claims parseToken1(String token) {
//        return Jwts.parserBuilder()
//                .setSigningKey(key)
//
//                .deserializeJsonWith((header, payload) -> {
//                    // Custom deserialization for the "authority" claim
//                    List<String> authorities = payload.get("authority", List.class);
//                    payload.put("authority", authorities);
//                    return payload;
//                })
//                .build()
//                .parseClaimsJws(token)
//                .getBody();
//    }
}
