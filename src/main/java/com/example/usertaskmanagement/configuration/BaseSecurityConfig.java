package com.example.usertaskmanagement.configuration;

import com.example.usertaskmanagement.configuration.security.AuthFilterConfigurerAdapter;
import com.example.usertaskmanagement.configuration.security.JwtAuthenticationEntryPoint;
import com.example.usertaskmanagement.configuration.security.TokenAuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.Customizer.withDefaults;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor

public class BaseSecurityConfig {


    private final TokenAuthService tokenAuthService;
    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;


    @Bean
    public SecurityFilterChain configFilterChain(HttpSecurity http) throws Exception {

//        http.authorizeHttpRequests(auth -> auth.requestMatchers(POST,"/user/register").permitAll());
//        http.authorizeHttpRequests(auth -> auth.requestMatchers(GET,"/admin/getAll").authenticated());
//        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());
//        csrf
        http.csrf(csrf -> csrf.disable());
//        cors

        http.cors(cors -> cors.disable());
//
        http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/admin/public").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers(POST, "/admin/register").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers(GET, "/admin/test").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers(POST,"/user/login").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers(GET, "/phone-register/getAll").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers(GET, "/admin/test-admin").hasAuthority("ADMIN"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers(GET, "/user/register").hasAuthority("ADMIN"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers(GET, "/user/test").hasAnyAuthority("USER","ADMIN"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers(GET, "/admin/test-super-admin").hasAuthority("SUPER_ADMIN"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/admin/user-admin").hasAnyAuthority("USER", "ADMIN"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/admin/admin").hasAuthority("ADMIN"));

        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());


        http.httpBasic(withDefaults());
        http.apply(new AuthFilterConfigurerAdapter(tokenAuthService));
        http.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint);
        return http.build();
    }




//    @Bean
//    protected InMemoryUserDetailsManager configure(AuthenticationManagerBuilder auth) throws Exception {
//        UserDetails user = User.withDefaultPasswordEncoder()
//                .username("user")
//                .password("password")
//                .roles("USER")
//                .build();
//        UserDetails admin = User.withDefaultPasswordEncoder()
//                .username("admin")
//                .password("password")
//                .roles("ADMIN")
//                .build();
//        return new InMemoryUserDetailsManager(user, admin);
//    }


}
