package com.example.usertaskmanagement.service;

import com.example.usertaskmanagement.dto.request.OrganizationRequest;
import com.example.usertaskmanagement.dto.response.OrganizationResponse;
import com.example.usertaskmanagement.model.Admin;
import com.example.usertaskmanagement.model.Organization;
import com.example.usertaskmanagement.repository.AdminRepository;
import com.example.usertaskmanagement.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final AdminRepository adminRepository;
    private final ModelMapper modelMapper;

    public OrganizationResponse registerOrganization(Long adminId, OrganizationRequest organizationRequest) {
        Admin admin = adminRepository.findById(adminId)
                .orElseThrow(() -> new RuntimeException(String.format("No such admin with id: %s", adminId)));
        Organization organization = modelMapper.map(organizationRequest, Organization.class);
        admin.setOrganization(organization);
        adminRepository.save(admin);
        return modelMapper.map(organization, OrganizationResponse.class);
    }

    public List<OrganizationResponse> getAllOrganizations() {
        List<Organization> organizationList = organizationRepository.findAll();
        List<OrganizationResponse> responseList = new LinkedList<>();
        for (int i = 0; i < organizationList.size(); i++) {
            responseList.add(modelMapper.map(organizationList.get(i), OrganizationResponse.class));
        }
        return responseList;
    }
}
