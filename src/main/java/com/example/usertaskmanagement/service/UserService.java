package com.example.usertaskmanagement.service;

import com.example.usertaskmanagement.configuration.security.JwtService;
import com.example.usertaskmanagement.dto.LoginResponse;
import com.example.usertaskmanagement.dto.UserLoginRequest;
import com.example.usertaskmanagement.dto.UserRegisterResponse;
import com.example.usertaskmanagement.dto.request.UserRequest;
import com.example.usertaskmanagement.dto.response.UserResponse;
import com.example.usertaskmanagement.model.User;
import com.example.usertaskmanagement.model.authority.GrantedUserAuthority;
import com.example.usertaskmanagement.model.authority.UserAuthority;
import com.example.usertaskmanagement.repository.UserAuthorityRepository;
import com.example.usertaskmanagement.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserAuthorityRepository userAuthorityRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    public UserRegisterResponse registerUser(UserRequest userRequest) {
        Optional<User> userOptional = userRepository.findByUsername(userRequest.getUsername());
        if (userOptional.isPresent()) {
            throw new RuntimeException();
        }
        GrantedUserAuthority userAuthority = userAuthorityRepository.findByAuthority(UserAuthority.USER)
                .orElseThrow(() -> new RuntimeException("authorite absent"));
        String generatedPassword = generateDefaultPassword();
        User user = User.builder()
                .name(userRequest.getName())
                .surname(userRequest.getSurname())
                .username(userRequest.getUsername())
                .email(userRequest.getEmail())
                .authorities(List.of(userAuthority))
                .password(passwordEncoder.encode(generatedPassword))
                .build();
        userRepository.save(user);
        return UserRegisterResponse.builder()
                .username(user.getUsername())
                .defaultPassword(generatedPassword)
                .build();
    }

    public List<UserResponse> findAll() {
        List<User> userList = userRepository.findAll();
        List<UserResponse> userResponseList = userList.stream()
                .map(user -> modelMapper.map(user, UserResponse.class))
                .collect(Collectors.toList());
        return userResponseList;
    }

    private String generateDefaultPassword() {
        StringBuilder password = new StringBuilder();
        int passwordLength = 8;

        // the characters that can be used in the password
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (int i = 0; i < passwordLength; i++) {
            int randomIndex = (int) (Math.random() * characters.length());
            password.append(characters.charAt(randomIndex));
        }

        if (isPasswordValid(password.toString())) {
            return password.toString();
        } else {
            throw new RuntimeException("not valid password generation");
        }


    }

    private boolean isPasswordValid(String password) {
        // Define the regex pattern for at least 6 alphanumeric characters
        String regexPattern = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$";

        // Use Pattern and Matcher to validate the password
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }


    public LoginResponse loginUser(UserLoginRequest userLoginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userLoginRequest.getUsername(), userLoginRequest.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        Optional<User> user = userRepository.findByUsername(userLoginRequest.getUsername());
        if (user.isPresent()) {

            return LoginResponse.builder()
                    .jwt(jwtService.issueTokenForUser(user.get()))
                    .build();
        }

        throw new RuntimeException("no user found");
    }
}
/*TODO
    fix registration logics in every service
    Add default password generation for user
    link users and organizations
    user starts task (It changes the status)
    users first login makes them change the default password (regex)
    add caching
    check use for Optional classes. You already user throw. why the need to check if exists
    admin-user?organization-user?
    fix findAll methods (use streams)
    phonenumberservice findall method n+1 can't be solved ask elmir
    change admin to adminUser, add admin entity for regulating website
    HttpMethod
*/

/* TODO
    admin getAll

 */