package com.example.usertaskmanagement.service;

import com.example.usertaskmanagement.dto.request.AddressRequest;
import com.example.usertaskmanagement.dto.response.AddressResponse;
import com.example.usertaskmanagement.model.Address;
import com.example.usertaskmanagement.model.Organization;
import com.example.usertaskmanagement.repository.AddressRepository;
import com.example.usertaskmanagement.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor

public class AddressService {
    private final AddressRepository addressRepository;
    private final OrganizationRepository organizationRepository;
    private final ModelMapper modelMapper;

    public AddressResponse create(Long organizationId, AddressRequest addressRequest) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(() -> new RuntimeException(String.format("No such organization id: %s", organizationId)));
        Address address = modelMapper.map(addressRequest, Address.class);

        organization.setAddress(address);
        organizationRepository.save(organization);
        return modelMapper.map(address, AddressResponse.class);
    }

    public AddressResponse updateAddress(Long addressId, AddressRequest addressRequest) {
                Address address = addressRepository.findById(addressId)
                        .orElseThrow(() -> new RuntimeException(String.format("No such address id: %s", addressId)));
        Optional<Address> addressOptional = Optional.ofNullable(address);
        addressOptional.ifPresent(address1 -> {
            address1.setAddress(addressRequest.getAddress());
            addressRepository.save(address1);
        });
        return modelMapper.map(address,AddressResponse.class);
    }
}
