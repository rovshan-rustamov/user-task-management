package com.example.usertaskmanagement.service;

import com.example.usertaskmanagement.configuration.security.JwtService;
import com.example.usertaskmanagement.dto.AdminLoginDto;
import com.example.usertaskmanagement.dto.LoginResponse;
import com.example.usertaskmanagement.dto.request.AdminRequest;
import com.example.usertaskmanagement.dto.response.AdminResponse;
import com.example.usertaskmanagement.dto.response.UserResponse;
import com.example.usertaskmanagement.exception.BadRequestException;
import com.example.usertaskmanagement.model.Admin;
import com.example.usertaskmanagement.model.authority.AdminAuthority;
import com.example.usertaskmanagement.model.authority.GrantedAdminAuthority;
import com.example.usertaskmanagement.repository.AdminAuthorityRepository;
import com.example.usertaskmanagement.repository.AdminRepository;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.usertaskmanagement.exception.ErrorCode.PASSWORDS_DIDNT_MATCH;
import static com.example.usertaskmanagement.exception.ErrorCode.USER_ALREADY_PRESENT;

@Service
@Data
public class AdminService {
    private final ModelMapper modelMapper;
    private final AdminRepository adminRepository;
    private final AdminAuthorityRepository authorityRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public LoginResponse registerAdmin(AdminRequest adminRequest) {
        Optional<Admin> adminOptional = adminRepository.findByUsername(adminRequest.getUsername());
        if (adminOptional.isPresent()) {
            throw new BadRequestException(USER_ALREADY_PRESENT, adminRequest.getUsername());
        }
        GrantedAdminAuthority adminAuthority = authorityRepository.findByAdminAuthority(AdminAuthority.ADMIN)
                .orElseThrow(() -> new RuntimeException("authority absent"));
        if (!adminRequest.getPassword().equals(adminRequest.getRepeatedPassword())) {
            throw new BadRequestException(PASSWORDS_DIDNT_MATCH);
        }
        Admin admin = Admin.builder()
                .username(adminRequest.getUsername())
                .email(adminRequest.getEmail())
                .password(passwordEncoder.encode(adminRequest.getPassword()))
                .authorities(List.of(adminAuthority))
                .build();
        adminRepository.save(admin);
        return LoginResponse.builder()
                .jwt(jwtService.issueTokenForAdmin(admin))
                .build();
    }


    public List<AdminResponse> findAll() {
        List<Admin> admins = adminRepository.findAll();
        return admins.stream()
                .map(admin -> modelMapper.map(admin, AdminResponse.class))
                .collect(Collectors.toList());
    }

    public LoginResponse loginAdmin(AdminLoginDto adminLoginDto) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(adminLoginDto.getUsername(), adminLoginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Optional<Admin> admin = adminRepository.findByUsername(adminLoginDto.getUsername());
        if (admin.isPresent()) {
            return LoginResponse.builder()
                    .jwt(jwtService.issueTokenForAdmin(admin.get()))
                    .build();
        }
        throw new RuntimeException("no user found");
    }


    public List<UserResponse> getAllUsers(Long id) {
        return null;
    }
}
/*

{
organization name
phone number
address

email
username
password

}


 */