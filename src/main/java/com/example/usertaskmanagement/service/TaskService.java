package com.example.usertaskmanagement.service;

import com.example.usertaskmanagement.dto.request.TaskRequest;
import com.example.usertaskmanagement.dto.response.TaskResponse;
import com.example.usertaskmanagement.model.Status;
import com.example.usertaskmanagement.model.Task;
import com.example.usertaskmanagement.model.User;
import com.example.usertaskmanagement.repository.TaskRepository;
import com.example.usertaskmanagement.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;


    public TaskResponse createTask(TaskRequest taskRequest) {
        Task task = modelMapper.map(taskRequest,Task.class);
        task.setStatus(Status.New);
        taskRepository.save(task);
        return modelMapper.map(task, TaskResponse.class);
    }

    public TaskResponse assignTaskToUser(Long userId, Long taskId){
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("No such user"));
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new RuntimeException("No such task"));
        user.getTasks().add(task);
        userRepository.save(user);
        return modelMapper.map(task, TaskResponse.class);
    }
    public List<TaskResponse> findAll() {
        List<Task> taskList = taskRepository.findAll();
        List<TaskResponse> responseList = new LinkedList<>();
        for (int i = 0; i < taskList.size(); i++) {
            responseList.add(modelMapper.map(taskList.get(i),TaskResponse.class));
        }

        return responseList;
    }
}
