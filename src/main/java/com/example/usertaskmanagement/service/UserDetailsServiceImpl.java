package com.example.usertaskmanagement.service;

import com.example.usertaskmanagement.model.Admin;
import com.example.usertaskmanagement.repository.AdminRepository;
import com.example.usertaskmanagement.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(()->new RuntimeException("no such username"));
    }
}

/* TODO change reference for user_authorities to granted_user_authority
 */
