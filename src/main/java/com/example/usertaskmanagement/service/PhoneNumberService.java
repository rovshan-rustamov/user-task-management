package com.example.usertaskmanagement.service;

import com.example.usertaskmanagement.dto.request.PhoneNumberRequest;
import com.example.usertaskmanagement.dto.response.PhoneNumberResponse;
import com.example.usertaskmanagement.model.Organization;
import com.example.usertaskmanagement.model.PhoneNumber;
import com.example.usertaskmanagement.repository.OrganizationRepository;
import com.example.usertaskmanagement.repository.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PhoneNumberService {
    private final OrganizationRepository organizationRepository;
    private final PhoneNumberRepository phoneNumberRepository;
    private final ModelMapper modelMapper;
    public PhoneNumberResponse registerPhoneNumber(Long organizationId, PhoneNumberRequest phoneNumberRequest) {
        Organization organization = organizationRepository.findById(organizationId)
                .orElseThrow(()-> new RuntimeException(String.format("No such organization with id: %s",organizationId)));
        PhoneNumber phoneNumber = modelMapper.map(phoneNumberRequest, PhoneNumber.class);
        phoneNumber.setOrganization(organization);
        organization.getPhoneNumbers().add(phoneNumber);
        phoneNumberRepository.save(phoneNumber);
        return modelMapper.map(phoneNumber,  PhoneNumberResponse.class);
    }

    public List<PhoneNumberResponse> getAllPhoneNumbers() {
        List<PhoneNumber> phoneNumbers = phoneNumberRepository.findAll();
        return phoneNumbers.stream()
                .map(phoneNumber -> modelMapper.map(phoneNumber, PhoneNumberResponse.class))
                .collect(Collectors.toList());
    }


}
