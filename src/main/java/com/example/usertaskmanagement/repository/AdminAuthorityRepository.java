package com.example.usertaskmanagement.repository;

import com.example.usertaskmanagement.model.authority.AdminAuthority;
import com.example.usertaskmanagement.model.authority.GrantedAdminAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminAuthorityRepository extends JpaRepository<GrantedAdminAuthority, Long> {
    Optional<GrantedAdminAuthority> findByAdminAuthority(AdminAuthority adminAuthority);
}
