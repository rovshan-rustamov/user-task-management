package com.example.usertaskmanagement.repository;

import com.example.usertaskmanagement.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrganizationRepository extends JpaRepository<Organization, Long> {
    @Query("select o from Organization o join fetch o.address a join fetch o.phoneNumbers")
    @Override
    List<Organization> findAll();
}
