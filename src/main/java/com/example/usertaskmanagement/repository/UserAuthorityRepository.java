package com.example.usertaskmanagement.repository;

import com.example.usertaskmanagement.model.authority.GrantedUserAuthority;
import com.example.usertaskmanagement.model.authority.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserAuthorityRepository extends JpaRepository<GrantedUserAuthority, Long> {

    Optional<GrantedUserAuthority> findByAuthority(UserAuthority authority);
}
