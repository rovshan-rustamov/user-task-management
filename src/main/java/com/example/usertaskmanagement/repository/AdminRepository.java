package com.example.usertaskmanagement.repository;

import com.example.usertaskmanagement.model.Admin;
import com.example.usertaskmanagement.model.authority.AdminAuthority;
import com.example.usertaskmanagement.model.authority.GrantedAdminAuthority;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, Long> {
    @Override
    @Query("select a from Admin a join fetch a.organization o join fetch o.phoneNumbers join fetch o.address")
    List<Admin> findAll();


    Optional<Admin> findByUsername(String username);
    //    @EntityGraph(value = "Admin.organization", type = EntityGraph.EntityGraphType.LOAD)
//    List<Admin> findAll();

}
