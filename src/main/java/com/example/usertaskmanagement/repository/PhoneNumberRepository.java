package com.example.usertaskmanagement.repository;

import com.example.usertaskmanagement.model.PhoneNumber;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {

    @Query("select p from PhoneNumber p join fetch p.organization o join fetch o.address")
//    @EntityGraph(value = "PhoneNumber.organization", type = EntityGraph.EntityGraphType.LOAD)
    @Override
    List<PhoneNumber> findAll();

//    @Query(value = "select\n" +
//            "    ph.id,\n" +
//            "    ph.phone_number,\n" +
//            "    ph.organization_id,\n" +
//            "    o.organization_name\n" +
//            "from phone_number ph\n" +
//            "left  outer join organization o on ph.organization_id = o.id" ,nativeQuery = true)
////    @EntityGraph(value = "PhoneNumber.organization", type = EntityGraph.EntityGraphType.LOAD)
//    List<PhoneNumber> findAll();

}


