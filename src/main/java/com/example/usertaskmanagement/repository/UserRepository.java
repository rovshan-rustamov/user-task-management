package com.example.usertaskmanagement.repository;

import com.example.usertaskmanagement.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);


    //    @Override
//    @Query("select u from User u join fetch u.tasks")
//    @EntityGraph(value = "User.tasks", type = EntityGraph.EntityGraphType.LOAD)
    @Query("select u from User u join fetch u.tasks join fetch u.authorities")

//    @Query("select u from User u")
    List<User> findAll();

    Optional<User> findByUsername(String username);



}
