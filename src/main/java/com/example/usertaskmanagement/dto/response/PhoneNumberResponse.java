package com.example.usertaskmanagement.dto.response;

import com.example.usertaskmanagement.model.Organization;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PhoneNumberResponse {
    Long id;
    String phoneNumber;
    String organizationName;
}
