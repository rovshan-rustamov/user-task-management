package com.example.usertaskmanagement.dto.response;

import com.example.usertaskmanagement.model.Status;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskResponse {
    Long id;
    String title;
    String description;
    String deadline;
    Status status;
}
