package com.example.usertaskmanagement.dto.response;

import com.example.usertaskmanagement.model.Task;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.LinkedList;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponse {
    Long id;
    String name;
    String surname;
    String email;
    List<Task> tasks = new LinkedList<>();
}
