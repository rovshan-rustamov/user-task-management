package com.example.usertaskmanagement.dto.response;

import com.example.usertaskmanagement.model.Address;
import com.example.usertaskmanagement.model.PhoneNumber;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationResponse {
    Long id;
    String organizationName;
    Address address;
    Set<PhoneNumber> phoneNumbers = new HashSet<>();
}
