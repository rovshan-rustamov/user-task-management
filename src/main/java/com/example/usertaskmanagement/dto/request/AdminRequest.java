package com.example.usertaskmanagement.dto.request;

import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdminRequest {


    String email;
    String username;
    @Size(min = 8,max = 16)
    String password;
    @Size(min = 8,max = 16)
    String repeatedPassword;
}


//    organization name
//    phone number
//    address
//
//            email
//    username
//            password