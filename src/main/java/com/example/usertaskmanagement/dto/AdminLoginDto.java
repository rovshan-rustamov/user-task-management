package com.example.usertaskmanagement.dto;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class AdminLoginDto {

    String username;
    @Pattern(regexp = "^(?=.*[A-Za-z0-9]).{6,}$")
    String password;
}
