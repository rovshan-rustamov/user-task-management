package com.example.usertaskmanagement.controller;

import com.example.usertaskmanagement.dto.LoginResponse;
import com.example.usertaskmanagement.dto.request.AdminRequest;
import com.example.usertaskmanagement.dto.AdminLoginDto;
import com.example.usertaskmanagement.dto.response.UserResponse;
import com.example.usertaskmanagement.service.AdminService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminController {
    private final AdminService adminService;


    @PostMapping("/register")
    public ResponseEntity<LoginResponse> registerAdmin(@RequestBody @Valid AdminRequest adminRequestDto) {

        return ResponseEntity.ok(adminService.registerAdmin(adminRequestDto));
    }

//    @PostMapping("/register")
//    public ResponseEntity<LoginResponse> loginAdmin(@RequestBody @Valid AdminLoginDto adminLoginDto){
//        return ResponseEntity.ok(adminService.loginAdmin(adminLoginDto));
//    }

    @GetMapping("/all-users/{id}")
    public ResponseEntity<List<UserResponse>> getAllUsers(@PathVariable Long id){
       return ResponseEntity.ok(adminService.getAllUsers(id));
    }



}


