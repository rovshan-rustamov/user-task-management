package com.example.usertaskmanagement.controller;

import com.example.usertaskmanagement.dto.request.AddressRequest;
import com.example.usertaskmanagement.dto.response.AddressResponse;
import com.example.usertaskmanagement.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @PostMapping("/{organizationId}")
    public AddressResponse create(@PathVariable Long organizationId
            ,@RequestBody AddressRequest request){
        return addressService.create(organizationId, request);
    }

    @PutMapping("/{organizationId}/{addressId}")
    public AddressResponse updateAddress(@PathVariable Long organizationId
            ,@PathVariable Long addressId
            ,@RequestBody AddressRequest addressRequest){
        return addressService.updateAddress(addressId,addressRequest);
    }

}
