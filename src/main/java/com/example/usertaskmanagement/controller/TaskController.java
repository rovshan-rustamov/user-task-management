package com.example.usertaskmanagement.controller;

import com.example.usertaskmanagement.dto.request.TaskRequest;
import com.example.usertaskmanagement.dto.response.TaskResponse;
import com.example.usertaskmanagement.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @PostMapping
    public TaskResponse createTask(@RequestBody @Validated TaskRequest taskRequest) {
        return taskService.createTask(taskRequest);
    }

    @PutMapping("/{userId}/{taskId}")
    public TaskResponse assignTask(@PathVariable Long userId, @PathVariable Long taskId) {
        return taskService.assignTaskToUser(userId, taskId);

    }

    @GetMapping
    public List<TaskResponse> getAllTasks() {
        return taskService.findAll();
    }


}
