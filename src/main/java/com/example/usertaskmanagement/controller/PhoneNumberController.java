package com.example.usertaskmanagement.controller;

import com.example.usertaskmanagement.dto.request.PhoneNumberRequest;
import com.example.usertaskmanagement.dto.response.PhoneNumberResponse;
import com.example.usertaskmanagement.service.PhoneNumberService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/phone-register")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class PhoneNumberController {
    PhoneNumberService phoneNumberService;

    @PostMapping("/{organizationId}")
    public PhoneNumberResponse registerPhoneNumber(@PathVariable Long organizationId,
                                                   @RequestBody PhoneNumberRequest phoneNumberRequest) {
        return phoneNumberService.registerPhoneNumber(organizationId, phoneNumberRequest);
    }

    @GetMapping("/getAll")
    public List<PhoneNumberResponse> getAllPhoneNumbers(){
        return phoneNumberService.getAllPhoneNumbers();
    }


}
