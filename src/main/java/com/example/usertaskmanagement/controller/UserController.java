package com.example.usertaskmanagement.controller;


import com.example.usertaskmanagement.configuration.security.SecurityService;
import com.example.usertaskmanagement.dto.LoginResponse;
import com.example.usertaskmanagement.dto.UserLoginRequest;
import com.example.usertaskmanagement.dto.UserRegisterResponse;
import com.example.usertaskmanagement.dto.request.UserRequest;
import com.example.usertaskmanagement.dto.response.UserResponse;
import com.example.usertaskmanagement.model.User;
import com.example.usertaskmanagement.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService userService;
    private final SecurityService securityService;

    @PostMapping("/register")
    public ResponseEntity<UserRegisterResponse> createUser(@RequestBody UserRequest userRequest){
        return ResponseEntity.ok(userService.registerUser(userRequest));
    }



    @GetMapping("/test")
    public ResponseEntity<String> test(){
        log.info("current user details {}", securityService.getJwtCredentials());

        return ResponseEntity.of(Optional.of("test"));
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> loginUser(@RequestBody UserLoginRequest userLoginRequest){
        return ResponseEntity.ok(userService.loginUser(userLoginRequest));
    }

    @GetMapping
    public List<UserResponse> getAllUsers(){
        return userService.findAll();
    }
}
