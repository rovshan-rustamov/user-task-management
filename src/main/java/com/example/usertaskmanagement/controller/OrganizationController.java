package com.example.usertaskmanagement.controller;

import com.example.usertaskmanagement.dto.request.OrganizationRequest;
import com.example.usertaskmanagement.dto.response.OrganizationResponse;
import com.example.usertaskmanagement.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/organization")
@RequiredArgsConstructor
public class OrganizationController {
    private final OrganizationService organizationService;


    @PostMapping("/{adminId}")
    public OrganizationResponse registerOrganization(@PathVariable Long adminId,
                                                     @RequestBody OrganizationRequest organizationRequest) {
        return organizationService.registerOrganization(adminId,organizationRequest);
    }

    @GetMapping
    public List<OrganizationResponse> getAllOrganizations(){
        return organizationService.getAllOrganizations();
    }
}
